# H-Client README [![GitLab CI](https://gitlab.com/Tardo/HClient/badges/master/build.svg)](https://gitlab.com/Tardo/HClient/pipelines)
H-Client its a mod for the game Teeworlds created and maintained by unsigned char*.

Please visit http://hclient.wordpress.com for up-to-date information about 
the mod.

Extra libraries used:
- libnotify (Only Linux)
- gdk_pixbuf-2.0 (Only Linux)
- libopus



# TEEWORLDS README
Copyright (c) 2011 Magnus Auvinen


This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.


Please visit http://www.teeworlds.com for up-to-date information about 
the game, including new versions, custom maps and much more.
